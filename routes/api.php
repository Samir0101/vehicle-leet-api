<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\V1\AuthController;
use App\Http\Controllers\API\V1\DriverController;
use App\Http\Controllers\API\V1\VehicleController;
use App\Http\Controllers\API\V1\RouteController;
use App\Http\Controllers\API\V1\SchedulerController;
use App\Http\Controllers\API\V1\Utils\UtilsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Rutas de inicio de sesión
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);

// Rutas de los Drivers
Route::apiResource('v1/drivers', DriverController::class)->middleware('auth:api') ;

// Rutas de los Vehicles
Route::apiResource('v1/vehicles', VehicleController::class)->middleware('auth:api');

// Rutas de los Routes
Route::apiResource('v1/routes', RouteController::class)->middleware('auth:api');

// Rutas de los Schedulers
Route::apiResource('v1/schedulers', SchedulerController::class)->middleware('auth:api');

// Rutas de los Utils
Route::controller(UtilsController::class)->middleware('auth:api')->group( function () {
    Route::get('v1/utils/getActiveDrivers', 'getActiveDrivers');
    Route::get('v1/utils/getActiveVehicles', 'getActiveVehicles');
    Route::get('v1/utils/getActiveRoutes', 'getActiveRoutes');
    Route::get('v1/utils/getActiveSchedulers', 'getActiveSchedulers');
});
