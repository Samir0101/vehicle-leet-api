<?php

namespace App\Http\Controllers\API\V1\Utils;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Driver;
use App\Models\Route;
use App\Models\Scheduler;
use App\Models\Vehicle;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UtilsController extends Controller
{
    public function getActiveDrivers(){
        try {
            $drivers = DB::table('drivers')
                ->select(DB::raw('id, first_name, last_name'))
                ->where('active',1)
                ->get();
            return response($drivers);
        } catch (\Throwable $th) {
            return response(['message' => 'Error en el servidor'], 500);
        }
    }

    public function getActiveVehicles(){
        try {
            $vehicles = DB::table('vehicles')
                ->select(DB::raw('id, description, capacity'))
                ->where('active',1)
                ->get();
            return response($vehicles);
        } catch (\Throwable $th) {
            return response(['message' => 'Error en el servidor'], 500);
        }
    }

    public function getActiveRoutes(){
        try {
            $routes = DB::table('routes')
                ->select(DB::raw('id, description'))
                ->where('active',1)
                ->get();
            return response($routes);
        } catch (\Throwable $th) {
            return response(['message' => 'Error en el servidor'], 500);
        }
    }

    public function getActiveSchedulers(){
        try {
            $schedulers = DB::table('schedulers')
                ->select('route_id','week_num','from','to')
                ->where('active',1)
                ->get();
            return response($schedulers);
        } catch (\Throwable $th) {
            return response(['message' => 'Error en el servidor'], 500);
        }
    }
}
