<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $vehicles = DB::table('vehicles')
                ->select(DB::raw('id,description,year,make,capacity,active'))
                ->get();
            return response($vehicles);
        } catch (\Throwable $th) {
            return response(['message' => 'Error en el servidor'], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'description' => 'required|max:255',
            'year' => 'required|max:20',
            'make' => 'required|max:30',
            'capacity' => 'required|max:20',
            'active' => 'required|max:1'
        ]);

        if($validator->fails()) {
            return response([
                'error' => $validator->errors(),
                'message' => 'Validation fail'
            ], 400);
        }

        try {
            $vehicle = Vehicle::create($data);
            return response(['message' => 'Created', 'vehicle' => $vehicle]);
        } catch (\Throwable $th) {
            return response(['message' => 'Error en el servidor'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function show($vehicleId)
    {
        try {
            $vehicleFound = DB::table('vehicles')->where('id', $vehicleId)->first();
            if(isset($vehicleFound)){
                return response(['vehicle' => $vehicleFound], 200);
            }
            return response(['message' => 'No vehicle found'], 404);
        } catch (\Throwable $th) {
            return response(['message' => 'Error en el servidor'], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $vehicle)
    {
        try {
            $vehicleUpdated = DB::table('vehicles')->where('id', $vehicle)->update($request->all());
            if($vehicleUpdated){
                return response([], 204);
            }
            return response(['message' => 'No vehicle found'], 404);
        } catch (\Throwable $th) {
            return response(['message' => 'Error en el servidor'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function destroy($vehicleId)
    {
        try {
            $vehicleDeleted = DB::table('vehicles')->where('id', $vehicleId)->delete();
            if($vehicleDeleted){
                return response([], 204);
            }
            return response(['message' => 'No vehicle found'], 404);
        } catch (\Throwable $th) {
            return response(['message' => 'Error en el servidor'], 500);
        }
    }
}
