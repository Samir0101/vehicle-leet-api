<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\Scheduler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class SchedulerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $schedulers = DB::table('schedulers')
                ->join('routes', 'schedulers.route_id', '=', 'routes.id')
                ->select('schedulers.id','route_id', 'routes.description as route_description','week_num','from','to','schedulers.active')
                ->get();
            return response($schedulers);
        } catch (\Throwable $th) {
            return response(['message' => 'Error en el servidor'], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'route_id' => 'required|max:5',
            'week_num' => 'required|max:20',
            'from' => 'required|max:255',
            'to' => 'required|max:255',
            'active' => 'required|max:1'
        ]);

        if($validator->fails()) {
            return response([
                'error' => $validator->errors(),
                'message' => 'Validation fail'
            ], 400);
        }

        try {
            $scheduler = Scheduler::create($data);
            return response(['message' => 'Created', 'scheduler' => $scheduler]);
        } catch (\Throwable $th) {
            return response(['message' => 'Error en el servidor'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Scheduler  $scheduler
     * @return \Illuminate\Http\Response
     */
    public function show($schedulerId)
    {
        try {
            $schedulersFound = DB::table('schedulers')->where('id', $schedulerId)->first();
            if(isset($schedulersFound)){
                return response(['scheduler' => $schedulersFound], 200);
            }
            return response(['message' => 'No scheduler found'], 404);
        } catch (\Throwable $th) {
            return response(['message' => 'Error en el servidor'], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Scheduler  $scheduler
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $schedulerId)
    {
        try {
            $schedulerUpdated = DB::table('schedulers')->where('id', $schedulerId)->update($request->all());
            if($schedulerUpdated){
                return response([], 204);
            }
            return response(['message' => 'No scheduler found'], 404);
        } catch (\Throwable $th) {
            return response(['message' => 'Error en el servidor'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Scheduler  $scheduler
     * @return \Illuminate\Http\Response
     */
    public function destroy($schedulerId)
    {
        try {
            $schedulerDeleted = DB::table('schedulers')->where('id', $schedulerId)->delete();
            if($schedulerDeleted){
                return response([], 204);
            }
            return response(['message' => 'No scheduler found'], 404);
        } catch (\Throwable $th) {
            return response(['message' => 'Error en el servidor'], 500);
        }
    }
}
