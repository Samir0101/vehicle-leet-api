<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class RouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $routes = DB::table('routes')
            ->join('drivers', 'routes.driver_id', '=', 'drivers.id')
            ->join('vehicles', 'routes.vehicle_id', '=', 'vehicles.id')
            ->select(DB::raw('routes.id, drivers.first_name,vehicles.description as vehicle_description,routes.description,routes.active'))
            ->get();
            return response($routes);
        } catch (\Throwable $th) {
            return response(['message' => 'Error en el servidor'], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'driver_id' => 'required|max:5',
            'vehicle_id' => 'required|max:5',
            'description' => 'required|max:255',
            'active' => 'required|max:1'
        ]);

        if($validator->fails()) {
            return response([
                'error' => $validator->errors(),
                'message' => 'Validation fail'
            ], 400);
        }

        try {
            $route = Route::create($data);
            return response(['message' => 'Created', 'route' => $route]);
        } catch (\Throwable $th) {
            return response(['message' => 'Error en el servidor'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Route  $route
     * @return \Illuminate\Http\Response
     */
    public function show($routeId)
    {
        try {
            $routeFound = DB::table('routes')->where('id', $routeId)->first();
            if(isset($routeFound)){
                return response(['driver' => $routeFound], 200);
            }
            return response(['message' => 'No route found'], 404);
        } catch (\Throwable $th) {
            return response(['message' => 'Error en el servidor'], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Route  $route
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $routeid)
    {
        try {
            $routesUpdated = DB::table('routes')->where('id', $routeid)->update($request->all());
            if($routesUpdated){
                return response([], 204);
            }
            return response(['message' => 'No route found'], 404);
        } catch (\Throwable $th) {
            return response(['message' => 'Error en el servidor'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Route  $route
     * @return \Illuminate\Http\Response
     */
    public function destroy($routeId)
    {
        try {
            $routeDeleted = DB::table('routes')->where('id', $routeId)->delete();
            if($routeDeleted){
                return response([], 204);
            }
            return response(['message' => 'No route found'], 404);
        } catch (\Throwable $th) {
            return response(['message' => 'Error en el servidor'], 500);
        }
    }
}
