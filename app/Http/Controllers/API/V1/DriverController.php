<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\Driver;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $drivers = DB::table('drivers')
                ->select(DB::raw('id, first_name, last_name, ssn, dob, address, city, phone, active'))
                ->get();
            return response($drivers);
        } catch (\Throwable $th) {
            return response(['message' => 'Error en el servidor'], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'first_name' => 'required|max:100',
            'last_name' => 'required|max:100',
            'ssn' => 'required|max:30',
            'dob' => 'required|max:20',
            'address' => 'required|max:100',
            'city' => 'required|max:50',
            'zip' => 'required|max:10',
            'phone' => 'required|unique:drivers|max:50',
            'active' => 'required|max:1'
        ]);

        if($validator->fails()) {
            return response([
                'error' => $validator->errors(),
                'message' => 'Validation fail'
            ], 400);
        }

        try {
            $driver = Driver::create($data);
            return response(['message' => 'Created', 'driver' => $driver]);
        } catch (\Throwable $th) {
            return response(['message' => 'Error en el servidor'], 500);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function show($driverId)
    {
        try {
            $driverFound = DB::table('drivers')->where('id', $driverId)->first();
            if(isset($driverFound)){
                return response(['driver' => $driverFound], 200);
            }
            return response(['message' => 'No driver found'], 404);
        } catch (\Throwable $th) {
            return response(['message' => 'Error en el servidor'], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $driverId)
    {
        try {
            $driverUpdated = DB::table('drivers')->where('id', $driverId)->update($request->all());
            if($driverUpdated){
                return response([], 204);
            }
            return response(['message' => 'No driver found'], 404);
        } catch (\Throwable $th) {
            return response(['message' => 'Error en el servidor'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function destroy($driverId)
    {
        try {
            $driverDeleted = DB::table('drivers')->where('id', $driverId)->delete();
            if($driverDeleted){
                return response([], 204);
            }
            return response(['message' => 'No driver found'], 404);
        } catch (\Throwable $th) {
            return response(['message' => 'Error en el servidor'], 500);
        }
    }
}
